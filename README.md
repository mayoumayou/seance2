# LAB2

## Description

Ce programme permet d'expliquer la notion de pointeur en programmation C
Le projet a ete realise dans le cadre du cours INF3135 Construction et
maintenance de logiciel de la session d’ete 2017 a l’Universite du Quebec
A Montreal.

## Auteur

AKA ZABO Hyacinthe

## Fonctionnement
Pour generer un executable du programme, il suffit d’entrer la commande
gcc -o pointeur pointeur.c
Puis on lance le programme a l’aide de la commande
make
