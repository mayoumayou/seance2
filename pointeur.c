#include<stdio.h>

void inverser(int *x,int *y){ // Fonction inverser
	
	int c;
	c=*x;
	*x=*y;
	*y=c;
}

int main(void){//Fonction main point d'entree de notre programme
	int a=1;
	int b=2;
	//int *p; pointeur p 
	//p=&a; pointe vers l'adresse de a
	printf(" a:%d\n b:%d\n",a,b);
	inverser(&a,&b);
	printf(" a:%d\n b:%d\n",a,b);
}
